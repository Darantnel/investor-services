from .models import Investor
from rest_framework import viewsets, permissions, generics, status
from .serializers import InvestorSerializer
from rest_framework.response import Response


class InvestorViewSet(viewsets.ModelViewSet):

    permission_classes = [
        permissions.IsAuthenticated
    ]

    serializer_class = InvestorSerializer

    def get_queryset(self):
        return self.request.user.investor.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class AddInvestorAPI(generics.GenericAPIView):
    serializer_class = InvestorSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        investor = serializer.save()
        return Response({
            'investor': InvestorSerializer(investor, context=self.get_serializer_context()).data
        })


class DeleteInvestorAPI(generics.GenericAPIView):
    serializer_class = InvestorSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if request.user:
            user = request.user
            if request.data.get("id"):
                investor_id = request.data.get("id")
                try:
                    investor = Investor.objects.get(pk=investor_id, owner=user.id)
                    investor.delete()
                    return Response(status=status.HTTP_200_OK)
                except Investor.DoesNotExist:
                    return Response(status=status.HTTP_404_NOT_FOUND)


class UpdateInvestorAPI(generics.GenericAPIView):
    serializer_class = InvestorSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            if request.user:
                user = request.user
                if request.data.get("id"):
                    investor_id = request.data.get("id")
                    name = request.data.get("name")
                    email = request.data.get("email")
                    phone = request.data.get("phone")
                    try:
                        investor = Investor.objects.get(pk=investor_id, owner=user.id)
                        investor.email = email
                        investor.phone = phone
                        investor.name = name
                        investor.save()
                        return Response(
                            {'investors': InvestorSerializer(investor, context=self.get_serializer_context()).data})
                    except Investor.DoesNotExist:
                        return Response(status=status.HTTP_404_NOT_FOUND)


class GetInvestorAPI(generics.GenericAPIView):
    serializer_class = InvestorSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get(self, request, *args, **kwargs):
        if request.user:
            user = request.user
            if request.data.get("id"):
                investor_id = request.data.get("id")
                try:
                    investor = Investor.objects.get(pk=investor_id, owner=user.id)
                    return Response({'investors': InvestorSerializer(investor, context=self.get_serializer_context()).data})
                except Investor.DoesNotExist:
                    return Response(status=status.HTTP_404_NOT_FOUND)
            else:
                try:
                    if user.is_superuser:
                        investors = Investor.objects.all()
                        return Response({'investors': InvestorSerializer(investors, context=self.get_serializer_context(), many=True).data})
                    investors = Investor.objects.get(owner=user.id)
                    return Response({'investors': InvestorSerializer(investors, context=self.get_serializer_context(), many=True).data})
                except Investor.DoesNotExist:
                    return Response(status=status.HTTP_404_NOT_FOUND)

