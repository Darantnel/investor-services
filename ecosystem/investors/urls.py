from .api import AddInvestorAPI, GetInvestorAPI, DeleteInvestorAPI, UpdateInvestorAPI
from django.urls import path


urlpatterns = [
    path('add_investor', AddInvestorAPI.as_view(),  name='add_investor'),
    path('get_investor', GetInvestorAPI.as_view(),  name='get_investor'),
    path('delete_investor', DeleteInvestorAPI.as_view(),  name='delete_investor'),
    path('update_investor', UpdateInvestorAPI.as_view(),  name='update_investor'),
]