from django.db import models
from accounts.models import Account as User
from phone_field import PhoneField


class Investor(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, default=True)
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=255)
    phone = PhoneField(blank=True, help_text='Contact phone number')


