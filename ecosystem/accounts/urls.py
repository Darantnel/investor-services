from rest_framework import routers
from django.urls import path
from .api import RegisterAPI, LoginAPI, UserAPI


urlpatterns = [
    path('user', UserAPI.as_view(), name='user'),
    path('login', LoginAPI.as_view(), name='login'),
    path('register', RegisterAPI.as_view(), name='register'),
]

